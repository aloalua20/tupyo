<!- 6.개표결과 ->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<%@ page import="java.sql.*,javax.sql.*, java.io.*"%>
<html>
<div style="border:1; padding:30px;">
<head>
<style type = "text/css">
table {
   margin:auto;
}   

h2 {
   text-align:center;
}   
</style>
	<table border=1 bordercolor=blue width="600" height="80" style="font-size:30; text-align:center;">
	<tr>
	<td width = 200 ><a href='A_01.jsp'>후보등록 </a></td>
	<td width = 200 ><a href='B_01.jsp'>투표 </a></td>
	<td width = 200 bgcolor='ffff00'><a href='C_01.jsp'>개표결과</a></td>
	</tr>
	</table>
</head>
<body>
<h1 align=center>&nbsp;&nbsp;후보 별 득표율</h1>
<%
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");
	Statement stmt = conn.createStatement();
	Statement stmt2 = conn.createStatement();
	//count(t.id): 후보 당 득표수
	ResultSet rset = 
	stmt.executeQuery("select h.giho, h.name, count(t.id), count(*) from hubo_table h left join tupyo_table t on h.giho = t.id group by h.giho order by h.giho;");	
	//count(*) : 전체 투표수
	ResultSet rset2 = stmt2.executeQuery("select count(*) from tupyo_table;");
	//totalpyo : 전체 투표수
	// 참고 : ResultSet의 필드는 while문 안에서만 사용 가능
	int totalpyo=0;
	while(rset2.next()) {
		totalpyo=rset2.getInt(1);
	}

%>
<table cellspacing=1 width=600  border=1 style="font-size:20;">
<form method = post action= C_02_2.jsp>
<%
//개표결과
	//rset의 데이터를 1번 필드부터 출력하는 while문
	while (rset.next()) {
%>
		<tr height = 50>
		<td width=200><p align=center>
		<a href='C_02_2.jsp?key=<%=rset.getInt(1)%>&name=<%=rset.getString(2)%>'><%=rset.getInt(1)%> <%=rset.getString(2)%></a></p></td>
<%
		//id: 후보번호
		int id = rset.getInt(1);
		//pyo: 후보 당 득표수
		int pyo = rset.getInt(3);
		//bar: 바 차트 길이 (이미지*득표수)
		
		//rate: pyo/totalpyo 득표율(후보 당 득표수/전체 투표수)
		double rate = Math.round(((double)pyo/(double)totalpyo*100));
		double bar=2.8*rate;
%>
		<!- width에 int bar 값 대입, img가 width만큼 출력 ->
		<td width=400><img src='bar.jpg' width=<%=bar%> height=20>  <%=pyo%> (<%=rate%>%)</td>
		</tr>
<%
	}
%>
</form>
</table>
<%
rset.close();
rset2.close();
stmt.close();
stmt2.close();
conn.close();
%>
</body>
</div>
</html>
<!- 2. 후보삭제 ->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<%@ page import="java.sql.*,javax.sql.*, java.io.*"%>
<html>
<div style="border:1; padding:30px;">
<head>
<!--에러 잡는 try-catch문-->
<%!      
   public static int extractErrorCode (SQLException sqlException) {
      int errorCode = sqlException.getErrorCode();
      SQLException nested = sqlException.getNextException();
      while(errorCode==0&&nested!=null){
         errorCode=nested.getErrorCode();
         nested=nested.getNextException();
      }
      return errorCode;
   }
%>
<style type = "text/css">
table {
   margin:auto;
   text-align: center;
}   
h2 {
   text-align:center;
}   
</style>
<table border=1 bordercolor=blue width="600" height="80" style="font-size:30; text-align:center;">
	<tr>
	<td width = 200 bgcolor='ffff00'><a href='A_01.jsp'>후보등록 </a></td>
	<td width = 200 ><a href='B_01.jsp'>투표 </a></td>
	<td width = 200 ><a href='C_01.jsp'>개표결과</a></td>
	</tr>
</table>
</head>
<body>
<%
try{
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase","root","aeae");
	Statement stmt = conn.createStatement();
	Statement stmt2 = conn.createStatement();
	Statement stmt3 = conn.createStatement();
	
	//A_01.jsp에서 key, name으로 전달한 파라메터 받아오기
	String gihobunho = request.getParameter("key");
	String huboja = request.getParameter("name");

	//stmt.executeQuery: recordSet 반환, select문에서 사용, 결과로 ResultSet반환
	//stmt.execute:성공한 row수 반환, insert, update, delete문에서 사용
	
	stmt.execute("delete from hubo_table where giho = "+gihobunho+";");	
	ResultSet rset = stmt2.executeQuery("select * from hubo_table;");
	//투표 테이블에서도 해당 데이터 삭제
	stmt3.execute("delete from tupyo_table where id = "+gihobunho+";");
%>
<br>
<h2><style="align:center;">후보삭제 결과: 기호번호 <%=gihobunho%>번 후보자 <%=huboja%> 이/가 삭제되었습니다.</h2>
<br>
<table cellspacing=1 width=600 height = 20 border=1 style="font-size:20;">
<%
	while(rset.next()) {
%>
		<tr style="height:50;">	
		<td width=200><p align=left>&nbsp;기호번호 : <%=rset.getInt(1)%></p></td>
		<td width=300><p align=left>&nbsp;후보명 : <%=rset.getString(2)%></p></td>
		</tr>
<%
	}
	rset.close();
	stmt.close();
	stmt2.close();
	stmt3.close();
	conn.close();
}
	catch(SQLException e) {
      if(extractErrorCode(e)==1062){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("데이터가 중복됩니다.");
         }else if(extractErrorCode(e)==1146){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("테이블이 존재하지 않습니다.");
         }else{
            out.println("[기타] <br>");
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.");
            out.println("후보자 이름이 깁니다.");
         }
   }

%>
</table>
</body>
</div>
</html>
<!- 5.투표결과 ->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<%@ page contentType = "text/html; charset=utf-8"%>
<%@ page import="java.sql.*,javax.sql.*, java.io.*"%>
<html>
<div style="border:1; padding:30px;">
<head>
<!--에러 잡는 try-catch문-->
<%!      
   public static int extractErrorCode (SQLException sqlException) {
      int errorCode = sqlException.getErrorCode();
      SQLException nested = sqlException.getNextException();
      while(errorCode==0&&nested!=null){
         errorCode=nested.getErrorCode();
         nested=nested.getNextException();
      }
      return errorCode;
   }
%>
<style type = "text/css">
table {
   margin:auto;
   text-align: center;
}   
h2 {
   text-align:center;
}   
</style>
	<table border=1 bordercolor=blue width="600" height="80" style="font-size:30; text-align:center;">
	<tr>
	<td width = 200 ><a href='A_01.jsp'>후보등록 </a></td>
	<td width = 200 bgcolor='ffff00'><a href='B_01.jsp'>투표 </a></td>
	<td width = 200 ><a href='C_01.jsp'>개표결과</a></td>
	</tr>
	</table>
</head>
<body>
<%
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase","root","aeae");
	Statement stmt = conn.createStatement();
	request.setCharacterEncoding("UTF-8");
try{
	String name = request.getParameter("name");
	String id = request.getParameter("hubo");
	String age = request.getParameter("age");
	String sql = "insert into tupyo_table values("+id+", "+age+");";
	stmt.execute(sql);

	stmt.close();
	conn.close();
	

%>
<br><br>
<h1 align=center> 투표결과: <%=id%>번 후보에 투표하였습니다. (연령대 그룹(<%=age%>0대)) </h1>
</body>
<%
}
catch(SQLException e) {
      if(extractErrorCode(e)==1062){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("데이터가 중복됩니다.");
         }else if(extractErrorCode(e)==1146){
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.<br>");
            out.println("테이블이 존재하지 않습니다.");
         }else{
            out.println("[기타] <br>");
            out.println(extractErrorCode(e)+" 에러가 발생했습니다.");
            out.println("후보자가 없습니다.");
         }
   }
%>
</div>
</html>
<!- 1. 후보등록 /삭제 ->
<!- hmtl에서 한글 설정, 브라우저에게 encoding형식 알림 ->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<!- jsp에서 한글 설정 ->
<!- @ : 페이지 지시 ->
<%@ page contentType = "text/html; charset=utf-8"%>
<!- jsp로 자바 파일 임포트
		java.sql.*: sql에 관련된 모든 자바 클래스 임포트
		javax.sql.*: JDBC 확장 클래스
		java.io.*: java.io 패키지, 데이터 입출력(input,output,reader,writer ->
<%@ page import="java.sql.*,javax.sql.*, java.io.*"%>
<html>
<div style="border:1; padding:30px;">
<head>
<!- css style태그로 페이지 전체 스타일 정의 ->
<style type = "text/css">
table {
   margin:auto;
   text-align: center;
}   
h2 {
   text-align:center;
}   
</style>
<!- 입력란에 공백 방지하는 자바스크립트 함수 ->
<script type="text/javascript">
function noSpace(obj) {
	var str_space = /\s/; //공백체크
	if(str_space.exec(obj.value)) { //공백이 있을 경우
		alert("해당 항목에는 공백을 사용할수 없습니다.\n\n공백은 자동적으로 제거 됩니다.");
		obj.focus();
		obj.value = obj.value.replace(' ',''); //공백제거
		 return false;
    }
}
</script>
<!- 테이블 1 생성 ->
<table border=1 bordercolor=blue width="600" height="80" style="font-size:30; text-align:center;">
	<tr>
	<td width = 200 bgcolor='ffff00'><a href='A_01.jsp'>후보등록 </a></td>
	<td width = 200 ><a href='B_01.jsp'>투표 </a></td>
	<td width = 200 ><a href='C_01.jsp'>개표결과</a></td>
	</tr>
	</table>
</head>
<body>
<!- jsp코드 시작 ->
<%
//MySQL드라이버 로딩: class클래스의 forName()메소드를 이용하여 interface driver 로드
	Class.forName("com.mysql.jdbc.Driver");
//MySQL Connection 객체 생성
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/seradatabase", "root","aeae");
//sql쿼리 생성/실행을 위한 statement객체 생성
	Statement stmt = conn.createStatement();
	Statement stmt2 = conn.createStatement();
//select sql쿼리 실행 결과물을 담는 ResultSet rset 생성
	ResultSet rset = stmt.executeQuery("select * from hubo_table;");
	
%>
<!- 테이블2 생성 ->
<table cellspacing=1 width=600 height = 20 border=1 style="font-size:20;">
<%
//후보자 삭제
	//ResultSet처리: resultSet으로 부터 원하는 데이터 추출하는 과정
	//rset.next()로 한 행씩 처리
	//rset의 데이터를 1번 필드부터 출력하는 while문
	//데이터 타입에 따라 rset.getInt 또는 rset.getString 사용 
	while (rset.next()) {
%>
<!- DB에 있는 후보자 리스트를 순서대로 출력 ->
	<!- = : jsp 출력 ->
	<!-	% : 지역변수, 로직 ->
	<!-	! : 전역변수, 함수 선언, 순서상관없음 ->
	
	<tr style="height:50;">	
	<td width=200><p align=left>&nbsp;기호번호 : <%=rset.getInt(1)%></p></td>
	<td width=300><p align=left>&nbsp;후보명 : <%=rset.getString(2)%></p></td>
	
	<!- onclick 사용하여 버튼 입력 시 A_02.jsp파일로 이동하며 파라메터(key, name) 전달 
		(A_02.jsp에서 request.getParameter()로 받음)->
	
	<td width=100><p align=center>
	<input type=button value=삭제 
	onClick=location.href='A_02.jsp?key=<%=rset.getInt(1)%>&name=<%=rset.getString(2)%>'></p></td>
	</tr>	
<%
	}
//후보자 등록
	String name = request.getParameter("name");
	int giho=0;
	  
	//rset2로 중간에 빈 기호번호 찾기
	 ResultSet rset2 = 
	 stmt2.executeQuery("select min(giho+1) from hubo_table where giho+1 not in (select giho from hubo_table);");
	//rset2의 결과 데이터에서 중간에 빈 기호번호 giho 생성
	 while(rset2.next()){
		 giho=rset2.getInt(1);
	 }
	 if(giho==0) giho=1;
%>
<!- form 태그를 이용하여 버튼 입력 시 A_03.jsp파일로 이동하며 파라메터(name=''에 들어오는 입력값) 전달 
	(A_03.jsp에서 request.getParameter()로 받음)->
	<form autocomplete=off method = post action= A_03.jsp>
	<tr style = "height:50;">
	<td width=200><p align=left>&nbsp;기호번호 : 
	<input bordercolor ='green' type='number' style="font-size:20; width:80;" 
	name='giho' value='<%=giho%>'></p></td>
	<td width=300><p align=left>&nbsp;후보명 : 
	<input style="font-size:20; width:160" type='text' 
	name='huboja' maxlength=20 required 
	pattern='^[가-힣a-zA-Z]+$' 
	onkeyup="noSpace(this);" onchange="noSpace(this);" ></p></td>
	<td width=100><p align=center><input style="bgcolor:green;" type=submit value=추가></p></td>
	</tr>
	</form>
	</table>
<%
	//자원반환
	rset.close();
	rset2.close();
	stmt.close();
	conn.close();
%>
</body>
</div>
</html>